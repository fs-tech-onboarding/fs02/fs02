import { Navbar, Nav, Container } from "react-bootstrap"
import { Link } from 'react-router-dom';


export default function AppNavbar(){

	return(
		<Navbar bg="light" expand="lg">
			<Container>
			<Navbar.Brand href="#home">Zuitt</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="ms-auto">
					<Nav.Link as={ Link } to="/">Home</Nav.Link>
					<Nav.Link as={ Link } to="/profile">Profile</Nav.Link>
				</Nav>
			</Navbar.Collapse>
			</Container>
		</Navbar>
		)
}