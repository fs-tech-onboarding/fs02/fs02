import {Row, Col, Button} from "react-bootstrap";

/*
	Pass-by-value (parameter passing):

	function fullname(myName){
		console.log(`Hello, I am ${myName}`)
	}

	Pass-by-reference (Object, Array)
*/ 

export default function Banner ({data}){
	
	const {title, content, label} = data

	return(
	<Row>
		<Col className="p-5 m-5 text-center">
		    <h1>{title}</h1>
		    {
			data.isAdmin === "True" ?
		    <></>
		    :
		    <p>{content}</p>		    	
		    }    	
		    <Button>{label}</Button>
		</Col>
	</Row>		
		)
}


