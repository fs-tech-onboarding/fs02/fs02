import './App.css';
import AppNavbar from "./components/AppNavbar"
import Home from "./pages/Home"
import Profile from "./pages/Profile"

import {BrowserRouter as Router} from 'react-router-dom'; 
import {Routes, Route} from 'react-router-dom';  


function App() {
  return (
    <Router>
    <AppNavbar/>
    <>
    <Routes> 
    <Route path="/" element={<Home/>} />
    <Route path="/profile" element={<Profile/>} />
    </Routes>
    </>
    </Router>
  );
}

export default App;
