import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

import 'bootstrap/dist/css/bootstrap.min.css';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

/*
  [SECTION] JSX (JavaScript + XML)
  JS + Extensible Mark-up Language
  - An extension of JS that let us create objects which then be compiled and added as HTML elements. 
  - With JSX, we are able to create HTML elements using JS. 
  .value // pass form elements
  .innerHTML // pass div, p, span, etc
  We no longer need to use value and HTML using JSX
  - JSX provides "syntactic sugar" for creating react components. 
  - Syntactic Sugar makes things easier to read and express. 
  - Syntax within a programming language that is designed to make things easier to read and or to express. 
  <Home /> JSX format yan kapag naka capital letter
  HTML Element using JavaScipt

*/ 

// const name = "Nehemiah Ellorico"
// const element = <h1>Hello, {name}</h1>

// root.render(element);
