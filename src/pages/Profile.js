import Banner from "../components/Banner"

export default function Profile(){

	let user = {
		firstName: "Nehemiah",
		lastName: "Ellorico",
		email: "nehemiah@mail.com",
		mobileNo: "0955-784-8789",
		isAdmin: "True"
	}

	let profileProp ={
		title: `Welcome, ${user.firstName} ${user.lastName}`,
		content: `email: ${user.email} | Mobile No.: ${user.mobileNo}`,		
		label: "Check Dashboard",
		isAdmin: `${user.isAdmin}`
	}

	return(
			<Banner data={profileProp} />
		)

	}

