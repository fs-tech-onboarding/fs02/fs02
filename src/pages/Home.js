import Banner from "../components/Banner"

export default function Home(){

	// props for the banner component
	let bannerProp ={
		title: "Zuitt Coding Bootcamp",
		content: "Opportunities for everyone, everywhere",
		label: "Enroll"
	}
	return(
			<Banner data={bannerProp} />
		)
}